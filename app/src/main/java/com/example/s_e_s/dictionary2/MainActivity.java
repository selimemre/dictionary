package com.example.s_e_s.dictionary2;

import android.app.Activity;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends Activity {

    TabHost tabHost;

    private EditText idText;
    private EditText pasText, edit1, edit2;
    private Button logIn, translate, delete, update, add;
    private RadioButton engtotr, trtoeng;
    private String path;
    private ListView listView;
    private RadioGroup radio;
    private SQLiteDatabase database = null;
    private LinearLayout adminpanel;
    Word words []= new Word [100];
    int counter =8;
    boolean check=false;
    boolean check2=false;
    int id = 12345;
    int pas= 6789;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adminpanel = (LinearLayout) findViewById(R.id.adminPanel);

        tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();
        TabHost.TabSpec tabSpec;
        //Tab 1
        tabSpec = tabHost.newTabSpec("tab1");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Admin Panel", null);
        tabHost.addTab(tabSpec);

        radio=(RadioGroup)findViewById(R.id.radiogroup1);

        //Tab2
        tabSpec = tabHost.newTabSpec("tab2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("Dictionary", null);
        tabHost.addTab(tabSpec);


        idText = (EditText) findViewById(R.id.Idtxt);
        pasText = (EditText) findViewById(R.id.passwordtxt);
        edit1 = (EditText) findViewById(R.id.edit1);
        edit2 = (EditText) findViewById(R.id.edit2);

        logIn = (Button) findViewById(R.id.logintBtn);
        translate = (Button) findViewById(R.id.translateBtn);
        add = (Button) findViewById(R.id.addBtn);
        delete = (Button) findViewById(R.id.deleteBtn);
        update = (Button) findViewById(R.id.updateBtn);


        engtotr = (RadioButton) findViewById(R.id.radioTurk);
        trtoeng = (RadioButton) findViewById(R.id.radioEng);


        tabHost = (TabHost) findViewById(R.id.tabhost);


        //-------------------------------------------------------
        listView = (ListView) findViewById(R.id.listview);
        File myDbPath = getApplication().getFilesDir();
        path = myDbPath + "/" + "CPMELAST";

        try {


            if (!databaseExist()) {

                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                Toast.makeText(this, "We have database", Toast.LENGTH_LONG).show();

                String mytable = "create table Dictionary ( "
                        + " recID integer PRIMARY KEY autoincrement, "
                        + "turkish text, "
                        + "english text);";
                database.execSQL(mytable); //we have table

                Toast.makeText(this, "We have table", Toast.LENGTH_LONG).show();
               fillWithDefaultWords();



                database.close();

                Toast.makeText(this, "We have one row of data", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(this, "We have a database already", Toast.LENGTH_LONG).show();
            }
        }
        catch (SQLException e) {
            //  msgBox.setText("Error while creating database");
        }


        //listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String temp = listView.getItemAtPosition(position).toString();

                String[] parts = temp.split("-");
                String part1 = parts[0]; // 004
                String part2 = parts[1]; // 034556
                edit1.setText(part1);
                edit2.setText(part2);

            }
        });



        makeList();
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                String take = rb.getText().toString();
                if (take.equals("TR to ENG")) {
                    check=true;
                    check2=false;
                }
                if (take.equals("ENG to TR")) {
                    check2=true;
                    check=false;
                }
            }
        });



    }
public void LogIn(View v){



    if ((Integer.parseInt(idText.getText().toString())==id) &&(Integer.parseInt(pasText.getText().toString())==pas) ){

        adminpanel.setVisibility(View.VISIBLE);
        Toast.makeText(this,"Giriş Başarılı",Toast.LENGTH_LONG).show();


    }
    else
        Toast.makeText(this,"Yanlış Giriş",Toast.LENGTH_LONG).show();



    tabHost.setCurrentTab(1);
}

public void Logout(View v){

    adminpanel.setVisibility(View.INVISIBLE);
    Toast.makeText(this,"Çıkış Başarılı",Toast.LENGTH_LONG).show();


}




    public void fillWithDefaultWords(){

        words[0]= new Word("simple","basit");
        words[1]= new Word("add","ekle");
        words[2]= new Word("delete","sil");
        words[3]= new Word("update","güncelle");
        words[4]= new Word("exit","çıkış");
        words[5]= new Word("maintanence","bakım");
        words[6]= new Word("hesitate","tereddüt etmek");
        words[7]= new Word("ignore","görmezden gelmek");


        for (int i=0 ; i<8 ; i++){
            String input = "insert into Dictionary (turkish, english) values ('" + words[i].tr() + "','" + words[i].eng() + "')";
            database.execSQL(input);

        }



    }


    private boolean databaseExist() {
        File dbFile = new File(path);
        return dbFile.exists();
    }

    public void makeList() {
        database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        String readData = "select * from Dictionary";
        Cursor cursor = database.rawQuery(readData, null);
        ArrayList<String> tableData = new ArrayList<>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tableData);
        while (cursor.moveToNext()) {
            String tr = cursor.getString(cursor.getColumnIndex("turkish"));
            String eng = cursor.getString(cursor.getColumnIndex("english"));
            String result = tr + "-" + eng;
            tableData.add(result);
        }

        listView.setAdapter(adapter);
        database.close();
    }


    public void Write(View v) {
     if (!edit1.getText().toString().equals("") && !edit2.getText().toString().equals("")){
         try {
             database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
             String tr2 = edit1.getText().toString();
             String eng2 = edit2.getText().toString();
             words[counter]=new Word(eng2,tr2);
             counter++;
             String input = "insert into Dictionary (turkish, english) values ('" + tr2 + "','" + eng2 + "')";
             database.execSQL(input);
             Toast.makeText(this, "one row is inserted", Toast.LENGTH_LONG).show();
             database.close();
         } catch (SQLException e) {

         }

     }else{ Toast.makeText(this, "Please fill all text areas", Toast.LENGTH_LONG).show();}
        makeList();


    }
    public void Update(View v) {

        if (!edit1.getText().toString().equals("") && !edit2.getText().toString().equals("")) {

            try {
                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                String tr2 = edit1.getText().toString();
                String eng2 = edit2.getText().toString();
                String input = "update Dictionary set turkish = '"+tr2+"' , english= '"+eng2+"' where turkish = '"+tr2+"' OR  english = '"+eng2+"'" ;
                database.execSQL(input);
                Toast.makeText(this, "one row is updated", Toast.LENGTH_LONG).show();
                database.close();
            } catch (SQLException e) {

            }
        }

        else{                Toast.makeText(this, "Please write word to Word Turkish", Toast.LENGTH_LONG).show();
        }
        makeList();

    }
    public void test (View v ){


    }
    public void TRNS (View v ){

        String tr2 = edit1.getText().toString();
        String eng2 = edit2.getText().toString();

if (check==true){
    Toast.makeText(this, "TR TO ENG", Toast.LENGTH_LONG).show();

    String input = "select english from Dictionary where turkish ='"+tr2+"'";
    database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
    Cursor cursor = database.rawQuery(input, null);
    while (cursor.moveToNext()){
        String eng = cursor.getString(cursor.getColumnIndex("english"));
        edit2.setText(eng);
    }

    database.close();



}
else if (check2==true){

    String input = "select turkish from Dictionary where english ='"+tr2+"'";
    database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
    Cursor cursor = database.rawQuery(input, null);
    while (cursor.moveToNext()){
        String tr = cursor.getString(cursor.getColumnIndex("turkish"));
        edit2.setText(tr);
    }

    database.close();
}


    }





    public void Delete(View v){

        if (!edit1.getText().toString().equals("")){
            try{
                database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.CREATE_IF_NECESSARY);
                String edt1 = edit1.getText().toString();

                String delete = "delete from Dictionary where turkish = '"+edt1+"' OR  english = '"+edt1+"' ";
                database.execSQL(delete);
                Toast.makeText(this, edt1+" is deleted from the database", Toast.LENGTH_LONG).show();
                database.close();
            }
            catch (SQLException e){

            }
        }
        else{
            Toast.makeText(this, "Please write a word in Word Turkish ", Toast.LENGTH_LONG).show();


        }

        makeList();
        edit1.setText("");
        edit2.setText("");

    }







}




