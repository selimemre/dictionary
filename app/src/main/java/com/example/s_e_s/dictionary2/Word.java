package com.example.s_e_s.dictionary2;

/**
 * Created by Selim on 20.12.2017.
 */

public class Word {
    String englismean;
    String Turkismean;
    public Word (String eng,String tr){
        englismean=eng;
        Turkismean=tr;
    }


    public String tr() {
        return Turkismean;
    }

    public String eng() {
        return englismean;
    }

    public void setEnglismean(String englismean) {
        this.englismean = englismean;
    }

    public void setTurkismean(String turkismean) {
        Turkismean = turkismean;
    }
}

